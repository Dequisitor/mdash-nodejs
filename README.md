# MDash Nodejs #

**Important:** *Application is currently under development (users page is not fully functional)*

Login/Signup web application written with the naive intent of trying out new things. The result is a cross-breed many would think is a bit unusual. **Node.js** webserver with **Express** utilizing **Coffeescript**, for persistence **MongoDB** with MongoJS. Client side using **Angular2** written in **Typescript** using transpilers. Additional tools, dependencies: npm, bower, typings, nodemon.

The whole application was written on a Raspberry Pi 2. UI is optimised for mobile devices (was tested on Nexus 5).

You can try the application here: http://frustramen.ddns.net:3000/ (my home raspberry)

## Running the application
Server and client are isolated from each other, they have different package.jsons. Steps needed to install dependencies:

- install nodejs v4.4.0+
- install npm v3.8.3+
- get bower (npm install -g bower)
- npm install (in main folder)
- bower install (in public folder)
- npm install (in public folder)

Optional:

- install nodemon (npm install -g nodemon)

To run the application:

- start mongodb if its not an already running service (e.g. mongod --dbpath db/)
- coffee server.coffee (in main directory)

For development:

- nodemon server.coffee (main dir) - watches changes, restarts on change
- npm run ntsc:w (in public folder - this transpiles typescript to javascript) - watch for ts files

## Settings
You can modify config.json in main directory to change port, db and secret.

### Addendum
- first time using angular2, I might be far from 'best practise'
- no tests yet, have to find a suitable testing framework for both server and client side